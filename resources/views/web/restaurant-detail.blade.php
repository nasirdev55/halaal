@extends('web.layouts.app')
@section('title',$restaurant)
@section('content')

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{$restaurant}}</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Restaurant</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1 style="color: #fd6e50;">About {{$restaurant}}</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-body text-center" style="box-shadow: 0.1rem 0.1rem 1rem rgb(0 0 0 / 20%);">
                        <h6>Albondigas Soup</h6>
                        <p class="small"> A steaming cup or bowl of Mexican meatball &amp; vegetable soup garnished with crispy tortilla strips and fresh cheddar-Jack cheese. If you like Mexican food try this!</p>
                        <span class="float-right font-weight-bold">
                            <i class="fas fa-star" style="color: #fd7e14"></i>
                            <i class="fas fa-star" style="color: #fd7e14"></i>
                            <i class="fas fa-star" style="color: #fd7e14"></i>
                            <i class="fas fa-star" style="color: #fd7e14"></i>
                        </span>
                        <h5 class="text-uppercase mt-1">CLOSED until Mon 14:00</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1 style="color: #fd6e50;">{{$restaurant}} Products</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-sm-12 col-xs-12 shop-content-right">
                    <div class="right-product-box">
                        <div class="product-categorie-box">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="{{'detail'}}">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Sale</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Chicket Rice.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Chicket Rice</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="new">New</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Editorial, Food & Drink.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Editorial, Food & Drink</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Sale</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Espetada.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Espetada</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="new">New</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Malai Botti.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Malai Botti</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Sale</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/McRoyal.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>McRoyal</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Sale</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Messi Burger.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Messi Burger</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Sale</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Patty Burger.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Patty Burger</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Sale</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Picof Tasty.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Picof Tasty</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <a href="shop-detail.html">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="new">New</p>
                                                        </div>
                                                        <img src="{{asset('assets/web/images/Tuna burgers.jpg')}}" class="img-fluid" alt="Image">
                                                    </div>
                                                    <div class="why-text">
                                                        <h4>Tuna burgers</h4>
                                                        <h5> £9.79</h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->
@endsection
