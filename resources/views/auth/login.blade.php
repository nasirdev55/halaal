@extends('web.layouts.app')
@section('title','Login')
@section('content')

    <div class="container">
        <div class="my-5">
            <div class="col-8 offset-2">
                <h1 class="title-left">Login Form</h1>
                <form class="mt-3 review-form-box collapse show" id="formRegister" style="" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="InputEmail1" class="mb-0">Email Address</label>
                            <input type="email" id="InputEmail1" placeholder="Enter Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <strong class="text-danger">{{ $message }}</strong>
                            @enderror
                        </div>
                        <div class="form-group col-md-12">
                            <label for="InputPassword1" class="mb-0">Password</label>
                            <input type="password" id="InputPassword1" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                                <strong class="text-danger">{{ $message }}</strong>
                            @enderror
                        </div>
{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                    <button type="submit" class="btn hvr-hover">Login</button>
{{--                    @if (Route::has('password.request'))--}}
{{--                        <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                            {{ __('Forgot Your Password?') }}--}}
{{--                        </a>--}}
{{--                    @endif--}}
                    <a href="{{ route('register') }}">Register?</a>
                </form>
            </div>
        </div>
    </div>
@endsection
