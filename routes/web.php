<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
//use App\Http\Controllers\Restaurant\RestaurantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::namespace('Restaurant')->prefix('restaurant')->as('restaurants.')->group(function (){
    Route::namespace('Auth')->group(function (){
//        Route::get('/login', [\App\Http\Controllers\Restaurant\Auth\LoginController::class, 'showLoginForm'])->name('showLoginForm');
//        Route::post('/login', [\App\Http\Controllers\Restaurant\Auth\LoginController::class, 'login'])->name('login');
//        Route::post('/logout', [\App\Http\Controllers\Restaurant\Auth\LoginController::class, 'logout'])->name('logout');
        Route::get('/register', [\App\Http\Controllers\Restaurant\Auth\RegisterController::class, 'showRegistrationForm'])->name('showRegistrationForm');
        Route::post('/register', [\App\Http\Controllers\Restaurant\Auth\RegisterController::class, 'register'])->name('register');

        Route::get('/login', [\App\Http\Controllers\Restaurant\AuthController::class, 'loginForm'])->name('showLoginForm');
        Route::post('/login', [\App\Http\Controllers\Restaurant\AuthController::class, 'login'])->name('login');
        Route::post('/logout', [\App\Http\Controllers\Restaurant\AuthController::class, 'logout'])->name('logout');
    });

    Route::middleware(['Restaurant'])->group(function(){

    Route::get('/', [\App\Http\Controllers\Restaurant\RestaurantController::class, 'index'])->name('home');
    Route::get('/edit/{restaurant}', [\App\Http\Controllers\Restaurant\RestaurantController::class, 'editProfile'])->name('editProfile');
    Route::post('/update-profile', [\App\Http\Controllers\Restaurant\RestaurantController::class, 'updateProfile'])->name('updateProfile');
    Route::get('/forget-password', [\App\Http\Controllers\Restaurant\ForgetPasswordController::class, 'forgetPassword'])->name('forgetPassword');
    Route::post('/new-password', [\App\Http\Controllers\Restaurant\ForgetPasswordController::class, 'newPassword'])->name('newPassword');





    // Category Route's
    Route::namespace('Categories')->prefix('category')->as('category.')->group(function (){
        Route::get('/',[\App\Http\Controllers\Restaurant\Categories\CategoryController::class,'index'])->name('list');
        Route::post('/store',[\App\Http\Controllers\Restaurant\Categories\CategoryController::class,'store'])->name('store');
        Route::post('/update',[\App\Http\Controllers\Restaurant\Categories\CategoryController::class,'update'])->name('update');
        Route::post('/delete',[\App\Http\Controllers\Restaurant\Categories\CategoryController::class,'destroy'])->name('delete');
        Route::post('/change-status',[\App\Http\Controllers\Restaurant\Categories\CategoryController::class,'changeStatus'])->name('changeStatus');
    });

    //    Deal Route's
    Route::namespace('Deal')->prefix('deal')->as('deal.')->group(function (){

        Route::get('/create',[App\Http\Controllers\Restaurant\Deal\DealController::class,'create'])->name('create');
        Route::get('/enable',[App\Http\Controllers\Restaurant\Deal\DealController::class,'enable'])->name('enable');
        Route::get('/disable',[App\Http\Controllers\Restaurant\Deal\DealController::class,'disable'])->name('disable');
        Route::post('/store',[\App\Http\Controllers\Restaurant\Deal\DealController::class,'store'])->name('store');
        Route::post('/update',[\App\Http\Controllers\Restaurant\Deal\DealController::class,'update'])->name('update');
        Route::post('/change-status',[\App\Http\Controllers\Restaurant\Deal\DealController::class,'changeStatus'])->name('changeStatus');
        Route::post('/delete',[\App\Http\Controllers\Restaurant\Deal\DealController::class,'destroy'])->name('delete');

    });
    //    Product Route's
    Route::namespace('Order')->prefix('order')->as('order.')->group(function (){

        Route::get('/order-pending',[App\Http\Controllers\Restaurant\Order\OrderController::class,'pending'])->name('pending-order');
        Route::get('/order-accepted',[App\Http\Controllers\Restaurant\Order\OrderController::class,'accepted'])->name('accepted-order');
        Route::get('/order-complete',[App\Http\Controllers\Restaurant\Order\OrderController::class,'complete'])->name('complete-order');
        Route::get('/order-rejected',[App\Http\Controllers\Restaurant\Order\OrderController::class,'rejected'])->name('rejected-order');
        Route::post('/order-change-status',[App\Http\Controllers\Restaurant\Order\OrderController::class,'changeStatus'])->name('changeStatus-order');
        Route::post('/order-accepted-status',[App\Http\Controllers\Restaurant\Order\OrderController::class,'acceptedStatus'])->name('acceptedStatus-order');

    });
    //    Product Route's
    Route::namespace('Products')->prefix('product')->as('product.')->group(function (){

        Route::get('/create',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'create'])->name('create');
        Route::get('/approved',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'approved'])->name('approved');
        Route::get('/rejected',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'rejected'])->name('rejected');
        Route::get('/pending',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'pending'])->name('pending');
        Route::post('/store',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'store'])->name('store');
        Route::post('/update',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'update'])->name('update');
        Route::post('/change-status',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'changeStatus'])->name('changeStatus');
        Route::post('/delete',[\App\Http\Controllers\Restaurant\Products\ProductController::class,'destroy'])->name('delete');

    });

    });

});

Route::namespace("Web")->group(function (){
    /* Checkout routes */
    Route::get("checkout",[\App\Http\Controllers\Web\CheckoutController::class,"index"])->name('index');
    Route::get("restaurant/{slug}",[\App\Http\Controllers\Web\RestaurantController::class,"details"])->name('restaurant-detail');
    Route::get("orders",[\App\Http\Controllers\Web\OrderController::class,"index"])->name('orders');
});

//demo
Route::get('/', function () {
    return view('web.home');
});
Route::get('about',function (){
   return view('web.about');
});
Route::get('contact',function (){
    return view('web.contact');
});

Route::get('cart',function (){
    return view('web.cart');
});


Route::get('detail',function (){
    return view('web.detailpage');
});

Route::get('categoryproducts',function (){
    return view('web.categoryproduct');
});

