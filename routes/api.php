<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('API\Restaurant')->prefix('restaurant')->as('restaurant.')->middleware('json.response')->group(function (){

    Route::post('register',[\App\Http\Controllers\API\Restaurant\AuthenticationController::class,'register'])->name('register');
    Route::post('login',[\App\Http\Controllers\API\Restaurant\AuthenticationController::class,'login'])->name('login');
    Route::post('forgetpassword',[\App\Http\Controllers\API\Restaurant\AuthenticationController::class,'forgetPassword'])->name('forgetPassword');
    Route::post('verify-otp',[\App\Http\Controllers\API\Restaurant\AuthenticationController::class,'verifyOtp'])->name('verifyOtp');
    Route::post('resend-otp',[\App\Http\Controllers\API\Restaurant\AuthenticationController::class,'resendOtp'])->name('resendOtp');

    Route::middleware(['auth:restaurant-api','Restaurant'])->group(function (){

        Route::post('change-password',[\App\Http\Controllers\API\Restaurant\AuthenticationController::class,'changePassword'])->name('changePassword');
        Route::post('get-profile',[\App\Http\Controllers\API\Restaurant\RestaurantController::class,'getProfile'])->name('getProfile');
        Route::post('edit-profile',[\App\Http\Controllers\API\Restaurant\RestaurantController::class,'editProfile'])->name('editProfile');
        Route::post('delete-profile',[\App\Http\Controllers\API\Restaurant\RestaurantController::class,'deleteProfile'])->name('deleteProfile');
        Route::post('account-details',[\App\Http\Controllers\API\Restaurant\RestaurantController::class,'accountDetails'])->name('accountDetails');
        Route::post('add-product',[\App\Http\Controllers\API\Restaurant\ProductController::class,'addProduct'])->name('addProduct');
        Route::post('delete-product',[\App\Http\Controllers\API\Restaurant\ProductController::class,'deleteProduct'])->name('deleteProduct');
        Route::post('edit-product',[\App\Http\Controllers\API\Restaurant\ProductController::class,'editProduct'])->name('editProduct');
        Route::post('product-details',[\App\Http\Controllers\API\Restaurant\ProductController::class,'productDetails'])->name('productDetails');
        Route::post('product-list',[\App\Http\Controllers\API\Restaurant\ProductController::class,'productList'])->name('productList');
        Route::post('featured-product-list',[\App\Http\Controllers\API\Restaurant\ProductController::class,'featuredProductList'])->name('featuredProductList');
        Route::post('add-featured-product',[\App\Http\Controllers\API\Restaurant\ProductController::class,'addFeaturedProduct'])->name('addFeaturedProduct');
        Route::post('delete-featured-product',[\App\Http\Controllers\API\Restaurant\ProductController::class,'deleteFeaturedProduct'])->name('deleteFeaturedProduct');
        Route::post('edit-featured-product',[\App\Http\Controllers\API\Restaurant\ProductController::class,'editFeaturedProduct'])->name('editFeaturedProduct');
        Route::post('featured-product-details',[\App\Http\Controllers\API\Restaurant\ProductController::class,'featuredProductDetails'])->name('featuredProductDetails');
        Route::post('dashboard',[\App\Http\Controllers\API\Restaurant\ProductController::class,'dashBoard'])->name('dashBoard');

//       Deals Route's
        Route::post('add-new-deal',[\App\Http\Controllers\API\Restaurant\DealController::class,'addNewDeal'])->name('addNewDeal');
        Route::post('delete-deal',[\App\Http\Controllers\API\Restaurant\DealController::class,'deleteDeal'])->name('deleteDeal');
        Route::post('edit-deal',[\App\Http\Controllers\API\Restaurant\DealController::class,'editDeal'])->name('editDeal');
        Route::post('deal-details',[\App\Http\Controllers\API\Restaurant\DealController::class,'dealDetail'])->name('dealDetail');
        Route::post('restaurant-deal-list',[\App\Http\Controllers\API\Restaurant\DealController::class,'restaurantDealList'])->name('restaurantDealList');

//        Route::post('test',[\App\Http\Controllers\API\Restaurant\CategoryController::class,'categoryList'])->name('test');

        // Orders Route's
//        Route::post('pending-orders',[\App\Http\Controllers\API\Restaurant\OrderController::class,'pendingOrders'])->name('pendingOrders');
        Route::post('order-detail',[\App\Http\Controllers\API\Restaurant\OrderController::class,'orderDetail'])->name('orderDetail');
        Route::post('order-list',[\App\Http\Controllers\API\Restaurant\OrderController::class,'orderList'])->name('orderList');
        Route::post('history',[\App\Http\Controllers\API\Restaurant\OrderController::class,'history'])->name('history');
        Route::post('history-online-payment',[\App\Http\Controllers\API\Restaurant\OrderController::class,'historyOnlinePayment'])->name('historyOnlinePayment');
        Route::post('history-cod',[\App\Http\Controllers\API\Restaurant\OrderController::class,'historyCOD'])->name('historyCOD');
        Route::post('order-status-change',[\App\Http\Controllers\API\Restaurant\OrderController::class,'orderStatusChange'])->name('orderStatusChange');
        Route::post('order-statuses',[\App\Http\Controllers\API\Restaurant\OrderController::class,'orderStatuses'])->name('orderStatuses');

//        Category Route's
        Route::post('category-list',[\App\Http\Controllers\API\Restaurant\CategoryController::class,'list'])->name('category-list');
        Route::post('edit-account',[\App\Http\Controllers\API\Restaurant\AccountController::class,'editAccount'])->name('editAccount');

    });




});
//Customer Side Api's Routes
Route::namespace('API\Customer')->prefix('customer')->as('customer.')->middleware('json.response')->group(function (){


    Route::post('register',[\App\Http\Controllers\API\Customer\AuthenticationController::class,'register'])->name('register');
    Route::post('login',[\App\Http\Controllers\API\Customer\AuthenticationController::class,'login'])->name('login');
    Route::post('forget-password',[\App\Http\Controllers\API\Customer\AuthenticationController::class,'forgetPassword'])->name('forgetPassword');
    Route::post('verify-otp',[\App\Http\Controllers\API\Customer\AuthenticationController::class,'verifyOtp'])->name('verifyOtp');
    Route::post('resend-otp',[\App\Http\Controllers\API\Customer\AuthenticationController::class,'resendOtp'])->name('resendOtp');

    Route::middleware(['auth:customer-api'])->group(function (){
        Route::post('change-password',[\App\Http\Controllers\API\Customer\AuthenticationController::class,'changePassword'])->name('changePassword');


    });


});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
