<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_no');
            $table->foreignId('cart_id')->constrained()->onDelete('cascade');
            $table->date('order_place_date')->default('2021-09-26');
            $table->decimal('total', 8, 2);
            $table->string('payment_status')->default('pending')->nullable();
            $table->string('charge_id')->nullable();
            $table->enum('payment_type', ['pickup', 'cod', 'card'])->default('pickup');
            $table->foreignId('status_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
