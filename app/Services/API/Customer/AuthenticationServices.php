<?php

namespace App\Services\API\Customer;

use App\Models\PasswordReset;
use App\Models\Restaurant;
use App\Models\User;
use App\Notifications\ForgetPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class AuthenticationServices
{
    public function register($request)
    {
        $customMsgs = [
            'name.required' => 'Please Provide Name',
            'email.required' => 'Please Provide Email',
            'phone.required' => 'Please Provide Phone',
            'address.required' => 'Please Provide Address',
            'password.required' => 'Please Provide Password',
//            'password_confirmation.required' => 'Please Provide Confirm Password',
        ];
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'phone' => 'required|integer',
                'address' => 'required',
                'password' => 'required|min:6',
                'password_confirmation' => 'required_with:password|same:password|min:6'
            ], $customMsgs
        );

        if ($validator->fails()) {
            return response()->json(['status' => 406, 'message' => $validator->messages()->first()], 406);
        }

        if ($request->has('email') && $request->has('password')) {

            $user = User::create($request->except('password') + ['password' => bcrypt($request->password)]);

            $success['token'] = $user->createToken('JustHalaal-' . rand(0, 9))->accessToken;

            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
//                'logo' => $path,
            ];

            return response()->json(['status' => 200, 'message' => "Registered Successfully",
                'data' => $data, 'token' => $success['token']], 200);

        } else {
            return response()->json(['status' => 404, 'message' => \Exception::getMessage()], 404);
        }
    }

    public function login($request)
    {
        $customMsgs = [
            'email.required' => 'Please Provide Email',
            'password.required' => 'Please Provide Password',
        ];
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
                'password' => 'required',

            ], $customMsgs
        );

        if ($validator->fails()) {
            return response()->json(['status' => 406, 'message' => $validator->messages()->first()], 406);
        }

        if (Auth::guard('web')->attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = User::where(['email' => Auth::user()->email])->first();
//
            $token = $user->createToken('justHalaal-' . rand(0, 9))->accessToken;

            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
//                'role' => $user->role->name,
            ];
            return response()->json(['status' => 200, 'message' => "Login Successfully",
                'data' => $data, 'token' => $token], 200);

        } else {
            return response()->json(['status' => 401, 'message' => "Invalid Login Credentials"], 401);

        }
    }

    public function forgetPassword($request)
    {
        $validator = Validator::make($request->all(), [

            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 406, 'message' => $validator->messages()->first()], 406);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['status' => 404, 'message' => "Email Not Found"], 404);
        }

        $confirmation_code = mt_rand(1000, 9999);
        PasswordReset::insert([
            'email' => $request->email,
//            'created_at' => Carbon::now(),
            'token' => $confirmation_code]);

        $data = [
            'confirmation_code' => $confirmation_code,
            'email' => $user->email,
        ];
        Notification::route('mail', env('MAIL_CLIENT'))->notify(new ForgetPassword($data));

        return response()->json(['status' => 200, 'message' => 'OTP Code is send to your Email Address',
            'email' => $user->email], 200);
    }

    public function verifyOtp($request)
    {
        $validator = Validator::make($request->all(), [

            'otp' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 406, 'message' => $validator->messages()->first()], 406);
        }

        $otp = PasswordReset::where('email', $request->email)->where('token', $request->otp)
            ->first();

        if (!$otp) {
            return response()->json(['status' => 401, 'message' => "Invalid OTP Code"], 401);
        }

        $otp->delete();

        return response()->json(['status' => 200, 'message' => "OTP Verified Successfully"], 200);
    }

    public function resendOtp($request)
    {
        $customMsgs = [
            'email.required' => 'Please provide Email',
        ];
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
            ], $customMsgs
        );

        if ($validator->fails()) {
            return response()->json(['status' => 406, 'message' => $validator->messages()->first()], 406);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['status' => 404, 'message' => "Email doe's not Exist"], 404);
        }

        $email = PasswordReset::where('email', $request->email)->first();
        if (!$email) {
            return response()->json(['status' => 404, 'message' => "Email doe's not exist"], 404);
        }

        $confirmation_code = mt_rand(1000, 9999);

        $email->token = $confirmation_code;
        $email->save();
        $data = [

            'confirmation_code' => $confirmation_code,
            'email' => $user->email,
        ];

        Notification::route('mail', env('MAIL_CLIENT'))->notify(new ForgetPassword($data));

        return response()->json(['status' => 200, 'message' => 'OTP Code is Resend to your Email Address',
            'email' => $user->email], 200);
    }

    public function changePassword($request)
    {
        $customMsgs = [
            'email.required' => 'Please Provide Email',
            'password.required' => 'Please Provide Password',
        ];
        $validator = Validator::make($request->all(),
            [
                'password' => 'required|min:6',
                'password_confirmation' => 'required_with:password|same:password|min:6'
            ], $customMsgs
        );

        if ($validator->fails()) {
            return response()->json(['status' => 406, 'message' => $validator->messages()->first()], 406);
        }

        Auth::user()->password = Hash::make($request->password);
        Auth::user()->save();

        return response()->json(['status' => 200, 'message' => "Password Updated Successfully"], 200);
    }
}
