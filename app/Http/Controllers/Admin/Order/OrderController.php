<?php

namespace App\Http\Controllers\Admin\Order;

use App\Http\Controllers\Controller;
use App\Services\Order\OrderServices;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:admin');
    }

    public function acceptedNew(Request $request,OrderServices $orderServicess)
    {
        return $orderServicess->acceptedNew($request);
    }

    public function pending(Request $request,OrderServices $orderServicess)
    {
        return $orderServicess->pending($request);
    }

    public function rejected(Request $request,OrderServices $orderServicess)
    {
        return $orderServicess->rejected($request);
    }

    public function complete(Request $request,OrderServices $orderServicess)
    {
        return $orderServicess->complete($request);
    }

    public function changeStatus(Request $request,OrderServices $orderServicess)
    {
        return $orderServicess->changeStatus($request);
    }

    public function acceptedStatus(Request $request,OrderServices $orderServicess)
    {
        return $orderServicess->acceptedStatus($request);
    }
}
