<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    public function details($slug)
    {
        $restaurant = '';
        $restaurants = ['yasir-broast'=>'Yasir Broast','desi-restaurant'=>'Desi Restaurant','gourmet'=>'Gourmet','glowfish-broast'=>'Glowfish Broast',
                        'dera-restaurant'=>'Dera Restaurant','family-restaurant'=>'Family Restaurant'];
        foreach ($restaurants as $key => $rest){
            if ($key == $slug){
                $restaurant = $rest;
            }
        }
        return view('web.restaurant-detail',compact('restaurant'));
    }
}
