<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = "orders";

    public function orderDetails()
    {
        return $this->hasOne(OrderDetail::class,'order_id');
    }

    protected $guarded = [];

    public function Statuses()
    {
        return $this->hasMany(Status::class,'id','status_id');
   }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function cart()
    {
        return $this->hasMany(Cart::class,'id');
    }

}
